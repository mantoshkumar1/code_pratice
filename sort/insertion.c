#include<stdio.h>

void insertion_sort(int arr[], int len, int index)
{
   int t_index = index,
       temp;

   if(len == 0 || arr == NULL) return;
   if(index == len) return;

   while(t_index != 0)
   {
      if(arr[t_index - 1] > arr[t_index])
      {
          temp = arr[t_index];
          arr[t_index] =  arr[t_index - 1];
          arr[t_index - 1] = temp;
          t_index--;      
      }
      else break;
   }

   insertion_sort(arr, len, index + 1);
}

int main()
{
   int i;
   int arr[] = {5, 1, 4, 10, 6, 2};
   int len = sizeof(arr)/sizeof(arr[0]);
   insertion_sort(arr, len, 1);

   printf("\n");

   for(i = 0; i < len; i++) printf("%d ", arr[i]);

   printf("\n");
   return 0;
}
