#include <stdio.h>

int min_val_pos(int arr[], int len, int start)
{
    int i;
    int temp = arr[start];
    int loc = start;

    for(i = start + 1; i < len; i++)
    {
       if(temp > arr[i]) 
       {
          loc = i;
          temp  = arr[i];
       }
    }
   
    return loc;
}

void selection(int arr[], int len, int index)
{
   int i, loc;

   for(i = 0; i < len - 1; i++)
   {
      loc = min_val_pos(arr, len, i);
      if(loc != i)
      {
         arr[loc] += arr[i];
         arr[i] = arr[loc] - arr[i];
         arr[loc] -= arr[i];
      }
   } 
}

int main()
{
   int i;
   int arr[] = {5,1,8,3,10,15};
   selection(arr, sizeof(arr)/sizeof(arr[0]), 0);

   printf("\nselection sort: \n");
   for(i = 0; i < sizeof(arr)/sizeof(arr[0]); ++i)
      printf("%d ", arr[i]);
   return 0;
}
