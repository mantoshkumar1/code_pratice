#include<stdio.h>

int bsearch(int arr[], int start, int end, int data)
{
   int mid = (start + end) / 2;
   if(start > end) return -1;
   if(arr[mid] == data) return mid + 1;

   if(data > arr[mid]) start = mid + 1;
   else end = end - 1;

   return bsearch(arr, start, end, data);
}

int bsearch2(int arr[], int data)
{
   int mid,
       size = sizeof(arr)/sizeof(arr[0]),
       start = 0,
       end = size - 1;

    while(start <= end)
    {
       mid = (start + end) / 2;
       if(arr[mid] == data) return mid + 1;
       
       if(data > arr[mid]) start = mid + 1;
       else end = mid - 1;
    }

    return -1;
}

int main()
{
  int arr[] = {3,4,8,10};

  int size = sizeof(arr)/sizeof(arr[0]);
  int data = 2;
  int pos =  bsearch(arr, 0, size - 1, data);
  if(pos == -1)
     printf("data does not exist");
  else
     printf("position of data = %d", pos);

  printf("\nMethod 2: \n");
  pos =  bsearch2(arr, data);
  if(pos == -1)
     printf("data does not exist");
  else
     printf("position of data = %d", pos);
  
  printf("\n");
  return 0;
}



