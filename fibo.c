#include<stdio.h>

void fibo(int n)
{
   int x = 0,
       y = 1,
       temp;

   if( n == 0) return;
   if( n == 1) { printf("%d", x); return; }
   
   printf("%d  %d ", x, y);
   
   n = n - 2;
   while(n != 0)
   {
      temp = y;
      y = x + y;
      x = temp;
      printf("%d ", y);
      n --;
   }
}

void printfibo(int n, int a, int b)
{
   int temp;
   if(n == 0) return;
 
   temp = b;
   b = a + b;
   a = temp;
   printf("%d ", b);
   printfibo(n - 1, a, b);  
}

void fibo2(int n)
{
   if(n == 0) return;
   if(n == 1) { printf("0 "); return;}
 
   printf("0 1 ");
   printfibo(n - 2, 0, 1);
}


int main()
{
  int n = 8;

  printf("fibo(%d)\n", n);
  fibo(n);
  printf("\n");

  printf("fibo2(%d)\n", n);
  fibo2(n);
  printf("\n");

}

