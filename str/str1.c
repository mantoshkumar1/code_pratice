#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void token(char *arr)
{
    char *tok;
    int len = strlen(arr);
    char *str = calloc(len + 1, sizeof(char));
    strcpy(str, arr);
    //strncpy(str, arr, len);

    printf("\ntoken are : \n");
    tok = strtok(str, " ");
    while(tok)
    {
       printf("%s ", tok);
       tok = strtok(NULL, " ");
       printf("\n");
    }
  
    printf("\n");
}

int main ()
{
   char src[50], dest[50];

   char *ptr;

   strcpy(src,  "This is source");
   strcpy(dest, "This is destination");

   //strncat(dest, src, 10);
    strcat(dest, src);

   printf("%s", dest);

   //tokenization
   token(dest);
   printf("original string : %s", dest); 

   printf("\n");
   ptr = strchr(dest, 'd');
   printf("using strchr : %s \n", ptr); 
   
   return(0);
}
