#include <stdio.h>
#include<string.h>
#include<stdlib.h>

void fun(char name[])
{
    printf("\ninside fun\n");
    printf("len = %d \n", strlen(name));
    printf("size = %d\n", sizeof(name)); //name is a pointer now :D
   
}

void fun2(char *name)
{
    printf("\ninside fun2\n");
    printf("len = %d \n", strlen(name));
    printf("size = %d\n", sizeof(name));   
}

int main(){
    char name[16];

    scanf("%s", name);
    printf("len = %d \n", strlen(name));
    printf("size = %d\n", sizeof(name));
    printf("%s", name);

    fun(name);
    fun2(name);

}
