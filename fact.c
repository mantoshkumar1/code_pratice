#include<stdio.h>

int fact(int n)
{
  if(n == 1 || n == 0) return 1;
  return n * fact(n - 1);
}

int fact2(int n)
{
   int f = 1;

   if(n == 0) return 1;

   while(n != 0)
   {
      f = f * n;
      n--;
   }

   return f;
}

int main()
{
  int n = 4;
  printf("fact(%d) = %d \n", n, fact(4));
  printf("fact2(%d) = %d \n", n, fact2(4));
}
