#include<stdio.h>
#include<stdlib.h>

typedef struct node_s {
	int data;
	struct node_s *next;
}node;

int main()  {
	node *head = NULL;
	node * temp1 = NULL, *temp2 = NULL;
	int num_nodes = 4;
	int i;
	
	//creating first node
	head = (node *) malloc(sizeof(node));
//        head-
	head->data = 0;
	temp1 = head;	
 	for(i = 1; i < num_nodes; i++) {
		temp2 = (node *) malloc(sizeof(node));
                temp2->next = NULL;
		temp2->data = i;

		temp1->next = temp2;
                temp1 = temp2;
	}

	//traversal
	temp1 = head;
	while(NULL != temp1) {
		printf("%d  " , temp1->data);
                temp1 = temp1->next;
	}

	return EXIT_SUCCESS;
}	
