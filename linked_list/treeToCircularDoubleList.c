/* Write a recursive function treeToList(Node root) that takes an ordered binary tree and rearranges the internal \
   pointers to make a circular doubly linked list out of the tree nodes. The "previous" pointers should be stored \
   in the "small" field and the "next" pointers should be stored in the "large" field. The list should be arranged \
   so that the nodes are in increasing order. Return the head pointer to the new list. The operation can be done \
   in O(n) time -- essentially operating on each node once.
*/

#include <stdio.h>
#include <stdlib.h>

typedef struct node {
      struct node *small;
      int data;
      struct node *large;
}Node;


Node * createNode(int data)
{
     Node *temp = (Node *)malloc(sizeof(Node));
     temp->data = data;
     temp->small = temp->large = NULL;
     return temp;
}

/* create a tree */
void createTree(Node **root, int data)
{
    if(*root == NULL) *root= createNode(data);
    else if(data > (*root)->data) createTree(&((*root)->large), data);
    else createTree(&((*root)->small), data);
}

void traverseTree(Node *root)
{
     if(root == NULL) return;
     traverseTree(root->small);
     printf(" %d  ", root->data);
     traverseTree(root->large);
}

void traverseCDList(Node *head)
{
    Node *temp = head;
    while(temp)
    {
        printf("%d  ", temp->data);
        temp = temp->large;
        if(head == temp) break;
    }
}

Node * TreeToList(Node *root)
{
    Node *temp1 = NULL, *temp2 = NULL;
    if(root == NULL) return root;
    if(root->small == NULL || root->large == NULL) return root;
 
    temp1 = TreeToList(root->small);
    if(temp1 != NULL)
    {
       root->small = temp1;
       temp1->large = root;
    }
    
    temp2 = TreeToList(root->large);
    if(temp2 != NULL)
    {
       root->large = temp2;
       temp2->small = root;
    }
    
    if(temp1 && temp2)
    {
       temp1->small = temp2;
       temp2->large = temp1;
    }

    return temp1;

}

int main()
{
     Node *root = NULL,
          *head = NULL;

     createTree(&root, 5);
     createTree(&root, 1);
     createTree(&root, 4);
     createTree(&root, 6);
     createTree(&root, 2);
     createTree(&root, 9);
     
     traverseTree(root);
     head = TreeToList(root);
 
     printf("traversing circular linked list \n");
     traverseCDList(head);

     return 0;
}


