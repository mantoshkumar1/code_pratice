#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

struct node {
    int data;
   struct node *next;
};

void reverse(struct node **head, struct node *curr, struct node *prev)
{
    if(curr == NULL) return; //empty list

    if(curr->next == NULL)
    {
        curr->next = prev;
        *head = curr;
        return;
    }   

    reverse(head, curr->next, curr);
    curr->next = prev;
}

void swap(struct node ** head)
{
     struct node *temp = *head,
                 *t    = NULL,
                 *next;
     
     if(temp == NULL) return;

     *head = (*head)->next;
     while(temp && temp->next)
     {
         next = temp->next->next;

         t = temp->next;
         temp->next = t->next;
         t->next = temp;
         temp = next;
     }

}

int getLen(struct node *head)
{
    if(head == NULL) return 0;
    if(head->next == NULL) return 1;
    return 1 + getLen(head->next);
}
void push(struct node **head, int data) //add at first pos
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->data = data;
    temp->next = *head;

    *head = temp;
}

void insert_after(struct node *node, int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->data = data;
    temp->next = node->next;
    node->next = temp;
}
void insert_end(struct node **head, int data)
{
     if(*head == NULL)
     {
          push(head, data);
          return;
     }

    struct node *temp = *head;
    //traverse till last node

     while(NULL != temp->next) 
     {
           temp = temp->next;
      }

   
    struct node *temp1 = (struct node *) malloc(sizeof(struct node));
    temp1->data = data;
    temp1->next = NULL;
    temp->next = temp1;
}

void display(struct node *head)
{
     printf("\n");     
     while(head) 
     {
           printf("%d   ", head->data);
           head = head->next;
      }
}

void delete_node(struct node **head, int data)
{
    struct node *temp = *head;
    
    //first node
    if(temp->data == data)
    {
        *head = temp->next;
        free(temp);
        return;
    }

    temp = temp->next;
    while(temp->next!=NULL)
    {
       if(temp->next->data == data)
       {
           struct node *t = temp->next->next;
           free(temp->next);
           temp->next = t;
           return;
       }
   
       temp = temp->next;
    }
}

/* Takes head pointer of the linked list and index
    as arguments and return data at index*/
int GetNth(struct node* head, int index)
{
    struct node* current = head;
    int count = 0; /* the index of the node we're currently
                  looking at */
    while (current != NULL)
    {
       if (count == index)
          return(current->data);
       count++;
       current = current->next;
    }
   
    /* if we get to this line, the caller was asking
       for a non-existent element so we assert fail */
    assert(0);              
}

int main() {

    //create a link list with elements 1, 2, 3
     struct node *temp3 = (struct node *) malloc(sizeof(struct node));
     temp3->data = 3;
     temp3->next = NULL;

    struct node * temp2 = (struct node *) malloc(sizeof(struct node));    
     temp2->data = 2;
     temp2->next = temp3;

/*
       struct node *temp1 = (struct node *) malloc(sizeof(struct node));
       if (NULL == temp1) return EXIT_FAILURE;
       temp1->data = 1;
       temp1->next = temp2;

       struct node *head = temp1;
*/
       struct node *head = (struct node *) malloc(sizeof(struct node));
       if (NULL == head) return EXIT_FAILURE;
       head->data = 1;
       head->next = temp2;

       display(head);

       //inserting in the beginning
       push(&head, 0);
       insert_after(temp2, 5);
       insert_end(&head, 6);
      
        display(head);
        //printf("%d  ", GetNth(head,8));
        
        //after deleting 3
        printf("\nafter deleting 3\n");
        delete_node(&head, 3);
        display(head);
        
        printf("\nafter reversing list\n");
        reverse(&head, head, NULL);
        display(head);
        
        printf("\nlen of list: %d \n", getLen(head));
        
        insert_end(&head, 10);
        display(head);

        printf("\nswapping list\n");
        swap(&head);
        display(head);

        return 0;
}

