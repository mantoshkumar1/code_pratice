#include <stdio.h>
#include <stdlib.h>

struct node 
{
    struct node *left;
    int data;
    struct node *right;
};


struct queue
{
     struct node *ptr;
     struct queue *next;
};

struct queue *rear = NULL;

struct node * newNode(int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->left = temp->right = NULL;
    temp->data = data;
    return temp;
}

struct queue * newQueueItem(struct node *tnode)
{
    struct queue *temp = (struct queue *) malloc(sizeof(struct queue));
    temp->ptr = tnode;
    temp->next = NULL;
    return temp;
}


void enqueue(struct queue **front, struct node *tnode)
{
    if(tnode == NULL) return;
  
    struct queue *temp = newQueueItem(tnode);

    if(*front == NULL)
    {
        *front = rear = temp;
        return;
    }

    rear->next = temp;
    rear = temp;
}

void dequeue(struct queue **front)
{
    struct queue *temp = *front;
    if(temp == NULL) return;
  
    *front = temp->next;
     free(temp);
}

void BFS(struct node  *root, struct queue **front)
{
    if(root == NULL) return;

    printf("%d  ", root->data);
    dequeue(front);
    
    enqueue(front, root->left);
    enqueue(front, root->right);

    if(*front) 
        BFS((*front)->ptr, front);
}

void BFS2(struct node *root)
{
   struct queue *front = NULL;

   if(root == NULL) return;
   enqueue(&front, root);

   while(front)
   {  
       printf("%d  ", front->ptr->data);
       enqueue(&front, front->ptr->left);
       enqueue(&front, front->ptr->right);
       dequeue(&front);
   }
}
//MEHOD 3

int height(struct node *root, int level)
{
    int Lh = level,
        Rh = level;

    if(root == NULL) return 0;
    if(root->left == NULL && root->right == NULL) return level;
    
    if(root->left)  Lh = height(root->left, level + 1);
    if(root->right) Rh = height(root->right, level + 1);
    
    return (Lh > Rh) ? Lh : Rh;
}

void printLevel(struct node *root, int level)
{
   if(root == NULL) return;
   if(level == 1) printf("%d  ", root->data);
   else
   {
      printLevel(root->left, level -1); 
      printLevel(root->right, level -1); 
   }   
}

void BFS3(struct node *root)
{
    int h = height(root, 1);
    int i;
   
    if(root == NULL) return;

    for(i = 1; i <= h; i++)
        printLevel(root, i);
}


int main()
{
    int path[100];

    struct node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);

    root->left->right->left = newNode(6);
    root->left->right->right = newNode(7);
    root->left->right->right->left = newNode(8);

    printf("BFS traverse Method 1:\n");
    struct queue *front = NULL;
    BFS(root, &front);

    printf("\nBFS traverse Method 2:\n");
    BFS2(root);

    printf("\nBFS traverse Method 3:\n");
    BFS3(root);
    
    return 0;



}
