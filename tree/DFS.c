#include <stdio.h>
#include <stdlib.h>

struct node 
{
    struct node *left;
    int data;
    struct node *right;
};


struct stack
{
     struct node *ptr;
     struct stack *next;
};


struct node * newNode(int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->left = temp->right = NULL;
    temp->data = data;
    return temp;
}

struct stack * newStackItem(struct node *tnode)
{
    struct stack *temp = (struct stack *) malloc(sizeof(struct stack));
    temp->ptr = tnode;
    temp->next = NULL;
    return temp;
}

void push(struct stack **top, struct node *tnode)
{
    struct stack *temp = NULL;
    if(tnode == NULL) return;

    if(*top == NULL)
    {
       *top = newStackItem(tnode);
       return;
    }

    temp = newStackItem(tnode);
    temp->next = *top;
    *top = temp;
}

void pop(struct stack **top)
{
   struct stack *temp;
   if(*top == NULL) return;
  
   temp = *top;
   *top = (*top)->next;
   free(temp);     
}

void printDFS(struct node *root, struct stack **top)
{
    struct node *temp;
    if(*top == NULL) return;
    if(root == NULL) return;

    temp = (*top)->ptr;
    printf("%d ", temp->data);
    pop(top);
    push(top, temp->left);
    push(top, temp->right);
    printDFS(root, top);
    
}

void DFS(struct node *root, struct stack **top)
{
    if(root == NULL) return;
    push(top, root);
    printDFS(root, top);
}

int main()
{
    struct stack *top = NULL;

    struct node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);

    root->left->right->left = newNode(6);
    root->left->right->right = newNode(7);
    root->left->right->right->left = newNode(8);


    printf("DFS traverse: \n");
    DFS(root, &top);
    printf("\n");

    
    return 0;



}
