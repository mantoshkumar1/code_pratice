#include <stdio.h>
#include <stdlib.h>

struct node 
{
    struct node *left;
    int data;
    struct node *right;
};


struct stack
{
     struct node *ptr;
     struct stack *next;
};


struct node * newNode(int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->left = temp->right = NULL;
    temp->data = data;
    return temp;
}

struct stack * newStackItem(struct node *tnode)
{
    struct stack *temp = (struct stack *) malloc(sizeof(struct stack));
    temp->ptr = tnode;
    temp->next = NULL;
    return temp;
}

void push(struct stack **top, struct node *tnode)
{
    struct stack *temp = NULL;
    if(tnode == NULL) return;

    if(*top == NULL)
    {
       *top = newStackItem(tnode);
       return;
    }

    temp = newStackItem(tnode);
    temp->next = *top;
    *top = temp;
}

void pop(struct stack **top)
{
   struct stack *temp;
   if(*top == NULL) return;
  
   temp = *top;
   *top = (*top)->next;
   free(temp);     
}

void SpiralBFS(struct node  *root, struct stack **RL, struct stack **LR, int rl)
{
    
    if(root == NULL) return;

    if(rl) push(RL, root);
    else push(LR, root);
  
    rl = !rl;

    while(*LR || *RL)
    {
      if(rl)
      {
         while(*LR != NULL)
         {
             push(RL, (*LR)->ptr->left);
             push(RL, (*LR)->ptr->right);
             printf("%d  ", (*LR)->ptr->data);
             pop(LR);
         }
         rl = !rl;
      }
      else 
      {
         while(*RL != NULL)
         {
            push(LR, (*RL)->ptr->right);
            push(LR, (*RL)->ptr->left);
            printf("%d  ", (*RL)->ptr->data);
            pop(RL);
         }
         rl = !rl;
      } 
    }
}

int main()
{
    struct stack *RL = NULL,
                 *LR = NULL;

    struct node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);

    root->left->right->left = newNode(6);
    root->left->right->right = newNode(7);
    root->left->right->right->left = newNode(8);


    printf("\nSpiral BFS traverse:\n");
    SpiralBFS(root, &RL, &LR, 1);
    printf("\n");

    
    return 0;



}
