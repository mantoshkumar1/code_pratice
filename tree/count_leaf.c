#include <stdio.h>
#include <stdlib.h>

struct node 
{
    struct node *left;
    int data;
    struct node *right;
};


struct node * newNode(int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->left = temp->right = NULL;
    temp->data = data;
    return temp;
}

void inorder(struct node *root)
{

   if(root == NULL) return;
   printf("%d ", root->data);
   inorder(root->left);
   inorder(root->right);
}

void BST(struct node **root, int data)
{
     struct node *t = *root;
     if(*root == NULL)
     {
         *root = newNode(data);
         return;
     }

     if(data <= t->data)
     {
        if(t->left == NULL)
          {
              t->left = newNode(data);
              return;
          }
            
        BST(&(t->left), data);
     }
     else
     {
        if(t->right == NULL)
        {
            t->right = newNode(data);
            return;
        }
        BST(&(t->right), data);
     }
}


int countleafs(struct node *root)
{
  static int count = 0;

  if(root == NULL) return 0;
  if(root->left == NULL && root->right == NULL)
  {
    count++;
    return count;
  }
 
  countleafs(root->left);
  countleafs(root->right);
  return count;
}

int main()
{

    struct node *root = NULL;
    struct queue *front = NULL;

    BST(&root, 5);
    BST(&root, 3);
    BST(&root, 7);
    BST(&root, 2);
    BST(&root, 4);
    BST(&root, 6);
    BST(&root, 8);

    printf("\ninorder traverse Method:\n");
    inorder(root);

    printf("\nNo of leafs: = %d \n", countleafs(root));
    
    printf("\n"); 
    return 0;



}
