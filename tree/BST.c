#include <stdio.h>
#include <stdlib.h>

struct node 
{
    struct node *left;
    int data;
    struct node *right;
};


struct queue
{
     struct node *ptr;
     struct queue *next;
};

struct queue *rear = NULL;

struct node * newNode(int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->left = temp->right = NULL;
    temp->data = data;
    return temp;
}

struct queue * newQueueItem(struct node *tnode)
{
    struct queue *temp = (struct queue *) malloc(sizeof(struct queue));
    temp->ptr = tnode;
    temp->next = NULL;
    return temp;
}


void enqueue(struct queue **front, struct node *tnode)
{
    if(tnode == NULL) return;
  
    struct queue *temp = newQueueItem(tnode);

    if(*front == NULL)
    {
        *front = rear = temp;
        return;
    }

    rear->next = temp;
    rear = temp;
}

void dequeue(struct queue **front)
{
    struct queue *temp = *front;
    if(temp == NULL) return;
  
    *front = temp->next;
     free(temp);
}

void BFS(struct node  *root, struct queue **front)
{
    if(root == NULL) return;

    printf("%d ", root->data);
    dequeue(front);
    
    enqueue(front, root->left);
    enqueue(front, root->right);

    if(*front) 
        BFS((*front)->ptr, front);
}

void BFS2(struct node *root)
{
   struct queue *front = NULL;

   if(root == NULL) return;
   enqueue(&front, root);

   while(front)
   {  
       printf("%d ", front->ptr->data);
       enqueue(&front, front->ptr->left);
       enqueue(&front, front->ptr->right);
       dequeue(&front);
   }
}

void BST(struct node **root, int data)
{
     struct node *t = *root;
     if(*root == NULL)
     {
         *root = newNode(data);
         return;
     }

     if(data <= t->data)
     {
        if(t->left == NULL)
          {
              t->left = newNode(data);
              return;
          }
            
        BST(&(t->left), data);
     }
     else
     {
        if(t->right == NULL)
        {
            t->right = newNode(data);
            return;
        }
        BST(&(t->right), data);
     }
}

int main()
{

    struct node *root = NULL;
    struct queue *front = NULL;

    BST(&root, 5);
    BST(&root, 3);
    BST(&root, 7);
    BST(&root, 2);
    BST(&root, 4);
    BST(&root, 6);
    BST(&root, 8);

    printf("BFS traverse Method 1:\n");
    BFS(root, &front);

    printf("\nBFS traverse Method 2:\n");
    BFS2(root);
    
    printf("\n"); 
    return 0;



}
