#include <stdio.h>
#include <stdlib.h>

struct node 
{
    struct node *left;
    int data;
    struct node *right;
};


struct node * newNode(int data)
{
    struct node *temp = (struct node *) malloc(sizeof(struct node));
    temp->left = temp->right = NULL;
    temp->data = data;
    return temp;
}

int tsize(struct node *root)
{
    int sum = 0;
    if(root == NULL) return 0;
 
    sum = tsize(root->left) + sum + 1;
    sum = tsize(root->right) + sum + 1;
    sum = sum - 1;
    return sum;
}

int tLevel(struct node * root, int level)
{
     int Llevel = level,
         Rlevel = level;

     if(root == NULL) return 0;

     if(root->left) Llevel = tLevel(root->left, level + 1);
     if(root->right) Rlevel = tLevel(root->right, level + 1);

     level = (Llevel > Rlevel) ? Llevel : Rlevel;
     return level;
}

void mirrorTree(struct node * root)
{
    if(root == NULL) return;
 
    struct node * temp = root->left;
    root->left = root->right;
    root->right = temp;
  
    mirrorTree(root->left);
    mirrorTree(root->right);
}

void printpath(struct node * root, int path[], int len)
{
    int i;
    if(root == NULL) return;
    else if(root->left == NULL && root->right == NULL)
    {
       path[len] = root->data;
       len++;

       for(i = 0; i < len; i++)
           printf("%d ", path[i]);

       printf("\n");
       return;
    }

    path[len] = root->data;
    printpath(root->left, path, len + 1);
    printpath(root->right, path, len + 1);
}

void printLevel(struct node *root, int height, int curr_h)
{
   if(root == NULL) return;
   else if(curr_h == height - 1) 
   {
      printf("%d  ", root->data);
      return;
   }

   printLevel(root->left, height, curr_h + 1); 
   printLevel(root->right, height, curr_h + 1); 
}

void BFS(struct node *root)
{
    int h = tLevel(root, 1);
    int i = 0;    

    for(i = 0; i <= h; i++)
       printLevel(root, i, 0);
}

int main()
{
    int path[100];

    struct node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);

    root->left->right->left = newNode(6);
    root->left->right->right = newNode(7);
    root->left->right->right->left = newNode(8);



    printf("size of tree = %d \n", tsize(root));
    printf("max level of tree = %d \n", tLevel(root, 1));

    //printf("mirror tree");
    //mirrorTree(root);

    printf("paths: \n");
    printpath(root, path, 0);

    printf("BFS traverse:\n");
    BFS(root);

    
    return 0;



}
